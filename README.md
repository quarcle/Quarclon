## Quarclon: Simple 2D and 3D Positioning System for multi-agent systems

### Description:
This ROS package is a positioning system for detecting UAV(s) in an area using external 2D dimensional camera and internal altimeter attached to the UAV(s). This system is developed from the WhyCon and SwarmCon by Tom Krajnik in [this repository](https://github.com/WhyCon/SwarmCon).

### Disclaimer:
*This package is still under development - it is still not fully functioning.*

### Operating System Requirement:
1. **Ubuntu 15.10 (Wily)**, **Ubuntu 16.04 (Xenial)** or **Debian 8 (Jessie)** (Tested)

### Dependencies:
1. **ROS Kinetic** for multi-threading and distributed implementation
2. **libsdl1.2-dev** for GUI
3. **libsdl-ttf2.0-dev** for displaying text in the GUI
4. **libncurses5-dev** for printing text in terminal
5. **guvcview** for camera setup

### Installation procedure:
1. **Open a terminal (*CTRL + ALT + T*)**: 
2. **Install full ROS Kinetic**, [click here for more details](http://wiki.ros.org/kinetic/Installation/Ubuntu):
    1. Setup your sources.list:
    ```
    ~$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    ```
    2. Set up your keys:
    ```
    ~$ sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
    ```
    3. Update the installed packages:
    ```
    ~$ sudo apt update
    ```
    4. Install full ROS Kinetic desktop:
    ```
    ~$ sudo apt-get install ros-kinetic-desktop-full
    ```
    5. Initialise rosdep:
    ```
    ~$ sudo rosdep init
    ~$ rosdep update
    ```
    6. Setup the launch source:
    ```
    ~$ echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
    ~$ source ~/.bashrc
    ```
    7. Install dependencies for building the ROS packages:
    ```
    ~$ sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential
    ```
    8. Close the opened terminal
3. **Setup local ROS workspace**
    1. Open a new terminal
    2. Create a catkin workspace:
    ```
    ~$ mkdir -p ~/catkin_ws/src
    ~$ cd ~/catkin_ws/
    ~$ catkin_make
    ```
    3. Setup the ROS source for the catkin workspace:
    ```
    ~$ echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
    ~$ source ~/.bashrc
    ```
    4. Close the opened terminal
    5. *Note: for more detailed explanation about ROS usage, click [here](http://wiki.ros.org/ROS/Tutorials)*
4. **Install the other dependencies**:
    1. Open a new terminal
    2. Update the available packages:
    ```
    ~$ sudo apt update
    ```
    3. Install the packages:
    ```
    ~$ sudo apt install libsdl2-dev
    ~$ sudo apt install libsdl-ttf2.0-dev
    ~$ sudo apt install libncurses5-dev
    ~$ sudo apt install guvcview
    ```
    4. Close the terminal
5. **Setup the *Quarclon* in your local workspace**:
    1. Open a new terminal
    2. Clone the [*Quarclon*](https://gitlab.com/quarcle/Quarclon.git) repository to your local workspace:
    ```
    ~$ cd catkin_ws/src/
    ~$ git clone https://gitlab.com/quarcle/Quarclon.git
    ```
    3. Compile the catkin workspace:
    ```
    ~$ cd ..
    ~$ catkin_make
    ```

### Operation procedure:
1. **Connect the camera to your computer**
2. **Open a terminal**
3. **Run ROS master**:
    ```
    ~$ roscore    
    ```
4. **Open another terminal**
5. **Execute the ROS node of the *Quarclon***:
    
    Command format: *rosrun quarclon quarclon /dev/video[NumCamera] [NumObject]*
    
    Where: [NumCamera] is the index of camera device detected by the computer. [NumObject] is the number of object that are going to be tracked.

    Example:

    ```
    ~$ rosrun quarclon quarclon /dev/video1 5
    ```
6. **Open another terminal**
7. **See the published ROS topics**:
    ```
    ~$ rostopic list
    ```
8. **The information about tracked objects are published to rostopic */robot_info***. To show the information, use:
    ```
    ~$ rostopic echo /robot_info
    ```
9. **To check the ROS message of the ROS topic**:
    ```
    ~$ rostopic type /robot_info
    ```

### Contact:
For more information, contact Hilton Tnunay (htnunay@gmail.com).